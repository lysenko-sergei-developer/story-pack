#!/usr/bin/env node

const path = require("path");
const git = require("simple-git")("");
const shell = require("shelljs");

(async () => {
  const url = process.argv[2];
  const gitUrl = url.replace("https://github.com/", "git@github.com:");
  const repName = url.substring(url.lastIndexOf("/") + 1, url.length);

  const x = await shell.rm(repName);
  await git.clone(gitUrl);

  shell.cd(repName);
  await shell.exec('npm install');
  await shell.exec("./node_modules/.bin/start-storybook -p 9000");
})();
